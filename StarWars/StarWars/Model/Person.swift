//
//  Person.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/5/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import Foundation

class Person : NSObject{
    //    MARK: Variables & Constants
    public var name = String()
    public var specie = String()
    public var nVehicles = Int()
    public var gender = String()
    public var homePlanet = String()
    public var homePlanetURL = String()
    public var specieURL = String()
    public var skinColor = String()
    public var vehicleList = [Vehicle]()
}
