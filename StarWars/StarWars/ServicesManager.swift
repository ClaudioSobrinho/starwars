//
//  ServicesManager.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/5/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

@objc protocol ServicesManagerDelegate: class {
    
    @objc optional func didGetPeople(count: Int, nextPageURL: String, charactersList: [Person])
    @objc optional func didGetMissingAtributes()
}

class ServicesManager {
    
    //    MARK: Variables & Constants
    static let sharedInstance = ServicesManager()
    
    public var delegate : ServicesManagerDelegate? = nil
    private let baseURL = "https://swapi.co/api/"
    private let peopleURL = "people"
    private let vehiclesURL = "vehicles"
    private var countTest = 0

    //    MARK: Functions
    /**
     * - Parameter id : Used in case a specific person is wanted, pass nil for complete list
     * - Parameter nextPageURL : Used in case a specific page result is intended, pass nil for the first page
     */
    func getPeople(id: Int?, nextPageURL: String?){
        var url = baseURL + peopleURL
        
        //A utilização desta validação em vez do famoso "if let" do swift é propositada e é para evitar a criação desnecessária de constantes e respectiva alocação de memória
        if (id != nil){
            url += String(id!)
        } else if (nextPageURL != nil){
            url = nextPageURL!
        } 
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                return .success
            }
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("✅getPeople✅")
                    default:
                        print("🔴getPeople🔴: \(status)")
                    }
                }
                if let result = response.result.value {
                    
//                    let JSON = result as! NSDictionary
//                    print(JSON)
                    let json = JSON(result)
                    
                    let resultsArray = json["results"]
                    let jsonNextPageURL = json["next"].stringValue
                    
                    var peopleArray = [Person]()
                    
                    for (index,resultsArrayElement):(String, JSON) in resultsArray{
                        let person = Person()
                        for (key,personDictionary):(String, JSON) in resultsArrayElement {
                            switch (key){
                            case "name":
                                person.name = personDictionary.stringValue
                            case "species":
                                person.specieURL = personDictionary[0].stringValue
                            case "gender":
                                person.gender = personDictionary.stringValue
                            case "skin_color":
                                person.skinColor = personDictionary.stringValue
                            case "homeworld":
                                person.homePlanetURL = personDictionary.stringValue
                            case "vehicles":
                                for (i,vehiclesArrayElement):(String, JSON) in personDictionary{
                                    self.getVehicleOf(person: person, url: vehiclesArrayElement.stringValue)
                                }
                                person.nVehicles = personDictionary.arrayValue.count
                            default:
                                print(key)
                            }
                        }
                        self.getSpecieOf(person: (person))
                        self.getHomeWorldOf(person: (person))
                        peopleArray.append(person)
                    }
                    
                    self.delegate?.didGetPeople?(count: resultsArray.arrayValue.count, nextPageURL: jsonNextPageURL, charactersList: peopleArray)
                }
        }
    }
    
    // The method was conceived this way in order to take advantage of the async properties of the Alamofire requests
    func getSpecieOf(person: Person){
        Alamofire.request(person.specieURL, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("✅getSpecie✅")
                    default:
                        print("🔴getSpecie🔴: \(status)")
                    }
                }
                if let result = response.result.value {
                    let json = JSON(result)
                    self.countTest += 1
                    print(self.countTest)
                    person.specie = json["name"].stringValue
                }
        }
    }
    
    // The method was conceived this way in order to take advantage of the async properties of the Alamofire requests
    func getHomeWorldOf(person: Person){
        Alamofire.request(person.homePlanetURL, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("✅getHomeWorld✅")
                    default:
                        print("🔴getHomeWorld🔴: \(status)")
                    }
                }
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    person.homePlanet = json["name"].stringValue
                }
        }
    }
    
    // The method was conceived this way in order to take advantage of the async properties of the Alamofire requests
    func getVehicleOf(person: Person, url: String){
        if url == "" {
            return
        }

        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
            }
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("✅getVehicle✅")
                    default:
                        print("🔴getVehicle🔴: \(status)")
                    }
                }
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    let vehicle = Vehicle()
                    vehicle.name = json["name"].stringValue
                    
                    person.vehicleList.append(vehicle)
                }
        }
    }
    
//    This was to avoid repeting the base structure of the request, but due to lack of time, wasn't implemented
    func baseRequest(url: String){
        
    }
}
