//
//  AboutViewController.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/17/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    //    MARK: IBOutlet
        @IBOutlet weak var linkedInButton: UIButton!
    
    //    MARK: IBActions
    @IBAction func didPressLinkedInButton(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "https://pt.linkedin.com/in/claudiosobrinho")! as URL)
    }
}
