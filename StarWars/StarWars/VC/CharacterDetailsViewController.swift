//
//  CharacterDetailsViewController.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/17/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import UIKit

class CharacterDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //    MARK: IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var homePlanetLabel: UILabel!
    @IBOutlet weak var skinColorLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var vehiclesLabelPlaceholder: UILabel!
    @IBOutlet weak var googleSearchTextView: UITextView!
    
    //    MARK: Variables & Constants
    public var person : Person? = nil
    
    //    MARK: VC
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        layoutVC()
        if person != nil {
            nameLabel.text = person?.name
            genderLabel.text = person?.gender
            homePlanetLabel.text = person?.homePlanet
            skinColorLabel.text = person?.skinColor
        }
    }
    
    //    MARK: IBActions
    @IBAction func didPressBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    //    MARK: Auxiliar Functions
    func layoutVC(){
        if person?.nVehicles == nil ||  person?.nVehicles == 0{
            vehiclesLabelPlaceholder.isHidden = true
            tableview.isHidden = true
        }

        let searchString = "http://www.google.com/search?q=" + person!.name.removingWhitespaces()
        let attributedString = NSMutableAttributedString(string: "Google Search")
        attributedString.addAttribute(.link, value: searchString, range: NSRange(location: 0, length: 13))
        
        googleSearchTextView.attributedText = attributedString
        googleSearchTextView.textAlignment = .center
        googleSearchTextView.font = UIFont.boldSystemFont(ofSize: 17)
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        return false
    }
    
    //    MARK: Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return person!.nVehicles
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VehicleTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "prototypeVehicleCell", for: indexPath) as! VehicleTableViewCell

        cell.nameLabel.text = person?.vehicleList[indexPath.row].name
        return cell
    }
}

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}
