//
//  CharactersListViewController.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/5/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import UIKit

class CharactersListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //    MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //    MARK: Variables & Constants
    let servicesManager = ServicesManager.sharedInstance
    let characterTableViewCellIdentifier = "CharacterTableViewCellIdentifier"
    var currentListCount = 0
    var currentListContent : [Person]? = nil
    var nextPageURL : String? = nil
    var tableViewDidFinishLoading = false
    
    //    MARK: VC
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        servicesManager.delegate = self
        servicesManager.getPeople(id: nil, nextPageURL: nil)
        
        tableView.register(UINib(nibName: "CharacterTableViewCell", bundle: nil), forCellReuseIdentifier: "CharacterTableViewCellIdentifier")
        
        tableView.rowHeight = 120
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK: Auxiliar Funtions
    func switchToTheDarkSide(bool: Bool){
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //    MARK: TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentListCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CharacterTableViewCell! = tableView.dequeueReusableCell(withIdentifier: characterTableViewCellIdentifier) as? CharacterTableViewCell
        
        if currentListContent != nil {
            cell.nameLabel.text = currentListContent![indexPath.row].name
            cell.specieLabel.text = currentListContent![indexPath.row].specie
            cell.vehicleCountLabel.text = String(currentListContent![indexPath.row].nVehicles)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "characterDetailSegue", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = currentListCount - 1
        if indexPath.row == lastElement && nextPageURL != "" {
            if !tableViewDidFinishLoading{
                tableViewDidFinishLoading = true
                self.tableView.reloadData()
            } else {
                servicesManager.getPeople(id: nil, nextPageURL: nextPageURL)
            }
        }
    }
    
    //    MARK:Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "characterDetailSegue"{
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem
            let destination = segue.destination as? CharacterDetailsViewController
            destination?.person = currentListContent![(tableView.indexPathForSelectedRow?.row)!]
        }
    }
}

//MARK: Delegates
extension CharactersListViewController : ServicesManagerDelegate{
    func didGetPeople(count: Int, nextPageURL: String, charactersList: [Person]) {
        if currentListContent == nil {
            currentListContent = charactersList
        } else {
            currentListContent?.append(contentsOf: charactersList)
        }
        
        currentListCount += count
        self.nextPageURL = nextPageURL
        
        self.tableView.reloadData()
    }
}

