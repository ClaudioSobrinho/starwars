//
//  CharacterTableViewCell.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/18/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {
    //    MARK: IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specieLabel: UILabel!
    @IBOutlet weak var vehicleCountLabel: UILabel!
}
