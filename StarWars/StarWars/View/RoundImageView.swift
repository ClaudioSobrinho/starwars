//
//  RoundImageView.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/17/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import UIKit

@IBDesignable
class RoundImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layoutIfNeeded()
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
}
