//
//  VehicleTableViewCell.swift
//  StarWars
//
//  Created by Claudio Sobrinho on 2/18/18.
//  Copyright © 2018 Claudio Sobrinho. All rights reserved.
//

import UIKit

class VehicleTableViewCell: UITableViewCell {
    //    MARK: IBOutlets
    @IBOutlet weak var nameLabel: UILabel!
}
